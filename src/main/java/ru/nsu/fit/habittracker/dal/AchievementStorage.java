package ru.nsu.fit.habittracker.dal;

import ru.nsu.fit.habittracker.entity.Achievement;
import ru.nsu.fit.habittracker.entity.Habit;

import java.util.List;

public interface AchievementStorage {
	boolean addAchievement(Achievement achievement);

	boolean alterAchievement(Achievement achievement);

	boolean deleteAchievement(Achievement achievement);

	List<Achievement> getAllAchievements();

	List<Achievement> getHabitAchievements(Habit habit);
}
