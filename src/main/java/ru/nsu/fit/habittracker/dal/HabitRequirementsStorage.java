package ru.nsu.fit.habittracker.dal;

import ru.nsu.fit.habittracker.entity.Habit;
import ru.nsu.fit.habittracker.entity.HabitRequirements;
import ru.nsu.fit.habittracker.entity.Period;

import java.util.List;

public interface HabitRequirementsStorage {
	boolean addRequirements(HabitRequirements habitRequirements);

	boolean alterRequirements(HabitRequirements habitRequirements);

	boolean deleteRequirements(HabitRequirements habitRequirements);

	List<HabitRequirements> getAllRequirements(Habit habit);

	List<HabitRequirements> getRequirementsWithinPeriod(Habit habit, Period period);
}
