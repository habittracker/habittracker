package ru.nsu.fit.habittracker.dal;

import ru.nsu.fit.habittracker.entity.Habit;

import java.util.List;

public interface HabitStorage {
	boolean addHabit(Habit habit);

	boolean alterHabit(Habit habit);

	boolean deleteHabit(Habit habit);

	List<Habit> getAllHabits();
}
