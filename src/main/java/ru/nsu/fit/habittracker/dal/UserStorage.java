package ru.nsu.fit.habittracker.dal;

import ru.nsu.fit.habittracker.entity.User;

import java.util.List;

public interface UserStorage {
	boolean addUser(User user);

	boolean alterUser(User user);

	boolean deleteUser(User user);

	List<User> getAllUsers();
}
