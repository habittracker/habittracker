package ru.nsu.fit.habittracker.dal;

import ru.nsu.fit.habittracker.entity.Habit;

import java.util.LinkedList;
import java.util.List;

public class InMemoryStorage implements HabitStorage {
	private List<Habit> habits = new LinkedList<>();

	@Override
	public boolean addHabit(Habit habit) {
		if(!habits.contains(habit)) {
			habits.add(habit);
			return true;
		}
		return false;
	}

	@Override
	public boolean alterHabit(Habit habit) {
		if(habits.contains(habit)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteHabit(Habit habit) {
		habits.remove(habit);
		return false;
	}

	@Override
	public List<Habit> getAllHabits() {
		return null;
	}
}
