package ru.nsu.fit.habittracker.dal;

import ru.nsu.fit.habittracker.entity.SameStatusPeriod;
import ru.nsu.fit.habittracker.entity.Habit;
import ru.nsu.fit.habittracker.entity.Period;

import java.util.List;

public interface SameStatusPeriodStorage {
	boolean addSameStatusPeriod(SameStatusPeriod failPeriod);

	boolean alterSameStatusPeriod(SameStatusPeriod failPeriod);

	boolean deleteSameStatusPeriod(SameStatusPeriod failPeriod);

	List<SameStatusPeriod> getAllSameStatusPeriods(Habit habit);

	List<SameStatusPeriod> getSameStatusPeriodsWithinPeriod(Habit habit, Period period);
}
