package ru.nsu.fit.habittracker.dal;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import ru.nsu.fit.habittracker.entity.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SqliteStorage implements UserStorage {
//	private final static Logger logger = LogManager.getLogger("Default");
	private String dbPrefix = "jdbc:sqlite:";
	private String dbUrl;
	private Connection conn;

	public SqliteStorage(String dbUrl) {
		this.dbUrl = dbUrl;

		try {
			conn = DriverManager.getConnection(dbPrefix + dbUrl);
			createTables();
		}
		catch(SQLException e) {
//			logger.catching(e);
		}
	}

	@Override
	public boolean addUser(User user) {
		return false;
	}

	@Override
	public boolean alterUser(User user) {
		return false;
	}

	@Override
	public boolean deleteUser(User user) {
		return false;
	}

	@Override
	public List<User> getAllUsers() {
		return null;
	}

	private void createTables() throws SQLException {
		final String createUsers = "CREATE TABLE IF NOT EXISTS users (\n" +
		                           "id integer PRIMARY KEY,\n" +
		                           "name text NOT NULL UNIQUE,\n" +
		                           "image_url text\n" +
		                           ");";
		final String createHabits = "CREATE TABLE IF NOT EXISTS habits (\n" +
		                            "id integer PRIMARY KEY,\n" +
		                            "name text NOT NULL,\n" +
		                            "cur_chain_length integer,\n" +
		                            "max_chain_length integer,\n" +
		                            "desired_chain_length integer\n" +
		                            ");";
		final String createAchievements = "CREATE TABLE IF NOT EXISTS achievements (\n" +
		                                  "id integer PRIMARY KEY,\n" +
		                                  "name text NOT NULL UNIQUE,\n" +
		                                  "points integer\n" +
		                                  ");";

//		Statement statement = conn.createStatement();
//		statement.execute(createUsers);
//		statement.execute(createHabits);
//		statement.execute(createAchievements);
	}
}
