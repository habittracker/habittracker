package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class IntroWindow extends JPanel {
    private JFrame frame;
    private WindowManager windowManager;


    public static void main(String[] args){
//        IntroWindow startWindow = new IntroWindow();
    }

    IntroWindow(WindowManager windowManager){
        this.windowManager = windowManager;
        JLabel heading = new JLabel("Habit Tracker");
        heading.setFont(new Font("Arial", Font.BOLD, 50));
        heading.setAlignmentX(Component.CENTER_ALIGNMENT);
        heading.setMaximumSize(new Dimension(322, 40));
        heading.setForeground(new Color(51, 51, 51));

        JLabel subHeading = new JLabel("Change your habits, organize your life!");
        subHeading.setFont(new Font("Arial", Font.BOLD, 30));
        subHeading.setAlignmentX(Component.CENTER_ALIGNMENT);
        subHeading.setMaximumSize(new Dimension(549, 25));
        subHeading.setForeground(new Color(51, 51, 51));

        JButton startButton = new JButton("Start!");
        startButton.setFont(new Font("Arial", Font.BOLD, 28));
        startButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        startButton.setMaximumSize(new Dimension(174, 60));
        startButton.setBackground(new Color(219, 76, 63));
        startButton.setForeground(new Color(255, 255, 255));
        startButton.setFocusPainted(false);

        Dimension rigidAreaSize1 = new Dimension(0, 3);
        Dimension rigidAreaSize2 = new Dimension(0, 18);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(Box.createVerticalGlue());
        this.add(heading);
        this.add(Box.createRigidArea(rigidAreaSize1));
        this.add(subHeading);
        this.add(Box.createRigidArea(rigidAreaSize2));
        this.add(startButton);
        this.add(Box.createVerticalGlue());

        startButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                windowManager.setCurrentWindow(1);
            }
        });

//        frame = new JFrame("Habit Tracker");
//        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        frame.setSize(968, 549);
//        frame.setResizable(false);
//        frame.getContentPane().setLayout(new BorderLayout());
//        frame.getContentPane().setVisible(true);
//        frame.getContentPane().add(startPanel, BorderLayout.CENTER);
//        frame.setBackground(new Color(204, 204, 204));
//        frame.setVisible(true);
    }


}

