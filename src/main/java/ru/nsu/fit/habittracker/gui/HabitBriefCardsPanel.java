package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HabitBriefCardsPanel extends JPanel {
    private final int PANEL_X = 269;
    private final int PANEL_Y = 0;
    private final int PANEL_WIDTH = 700;
    private final int PANEL_HEIGHT = 588;
    private final Color PANEL_BACKGROUND = new Color(204, 204, 204);

    private final int CREATE_BUTTON_X = 602;
    private final int CREATE_BUTTON_Y = 456;
    private final int CREATE_BUTTON_WIDTH = 50;
    private final int CREATE_BUTTON_HEIGHT = 50;

    private WindowManager windowManager;

    HabitBriefCardsPanel(WindowManager wm){
        windowManager = wm;
        ImageIcon plusIcon = new ImageIcon("resources/plus.png", "plus button");
        JButton createHabitButton = new JButton(plusIcon);
        createHabitButton.setBounds(CREATE_BUTTON_X, CREATE_BUTTON_Y, CREATE_BUTTON_WIDTH, CREATE_BUTTON_HEIGHT);
        createHabitButton.setOpaque(false);
        createHabitButton.setContentAreaFilled(false);
        createHabitButton.setBorderPainted(false);
        createHabitButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e) {
                windowManager.setCurrentWindow(2);
            }
        });

        this.setBackground(PANEL_BACKGROUND);
        this.setSize(PANEL_WIDTH, PANEL_HEIGHT);
        this.setBounds(PANEL_X, PANEL_Y, PANEL_WIDTH, PANEL_HEIGHT);
        this.setLayout(null);
        this.add(createHabitButton);
    }
}
