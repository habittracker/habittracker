package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class SidePanel extends JPanel {
    private JFrame frame;
    private LinkedList<Component> components;

    private final int PANEL_X = 0;
    private final int PANEL_Y = 0;
    private final int PANEL_WIDTH = 244;
    private final int PANEL_HEIGHT = 547;


    public static void main(String[] args){
        LinkedList<Component> components = new LinkedList<>();
        ImageIcon userAvatar = new ImageIcon("resources/avatar2.png", "avatar");
        components.add(new PersonalInfoPanel("Michael", userAvatar));
        components.add(new MenuPanel(MenuPanel.MenuElement.Habits));
        SidePanel sidePanel = new SidePanel(components);
    }

    SidePanel(LinkedList<Component> components){
        this.components = components;

        this.setBackground(new Color(250, 250, 250));
        this.setSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

        this.setLayout(null);
        for(Component component : components){
            this.add(component);
        }
        this.setBounds(PANEL_X, PANEL_Y, PANEL_WIDTH, PANEL_HEIGHT);

//        frame = new JFrame("Habit Tracker");
//        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        frame.setSize(968, 549);
//        frame.setResizable(false);
//        frame.getContentPane().setLayout(null);
//        frame.getContentPane().setVisible(true);
//        frame.getContentPane().add(this);
//        frame.setBackground(new Color(204, 204, 204));
//        frame.setVisible(true);
    }
}
