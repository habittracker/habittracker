package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class MenuPanel extends JPanel {
    private JFrame frame;

    private final int PANEL_X = 0;
    private final int PANEL_Y = 224;
    private final int PANEL_WIDTH = 244;
    private final int PANEL_HEIGHT = 240;
    private final Color BACKGROUND = new Color(204, 204, 204);


    static enum MenuElement {
        Habits, Achievements, Settings, LogOut
    }
    private MenuElement activeMenuElement;
    private HashMap<MenuElement, Color> menuElementBackgroundColors = new HashMap<>();
    private HashMap<MenuElement, Color> menuElementTextColors = new HashMap<>();
    private Color notActiveBackgroundColor = new Color(250, 250, 250);
    private Color notActiveTextColor = new Color(51, 51, 51);
    private Color activeBackgroundColor = new Color(219, 76, 43);
    private Color activeTextColor = new Color(255, 255, 255);

    public static void main(String[] args){
        MenuPanel menuPanel = new MenuPanel(MenuElement.Habits);
    }

    MenuPanel(MenuElement activeMenuElement){
        this.activeMenuElement = activeMenuElement;

        for(MenuElement menuElement : MenuElement.values()){
            menuElementBackgroundColors.put(menuElement, notActiveBackgroundColor);
            menuElementTextColors.put(menuElement, notActiveTextColor);
        }
        menuElementBackgroundColors.put(activeMenuElement, activeBackgroundColor);
        menuElementTextColors.put(activeMenuElement, activeTextColor);

        JButton menuElement1 = new JButton("Habits");
        menuElement1.setForeground(menuElementTextColors.get(MenuElement.Habits));
        menuElement1.setBackground(menuElementBackgroundColors.get(MenuElement.Habits));
        menuElement1.setBorderPainted(false);
        menuElement1.setFont(new Font("Arial", Font.BOLD, 25));
        menuElement1.setAlignmentX(Component.CENTER_ALIGNMENT);
        menuElement1.setMaximumSize(new Dimension(244, 60));

        JButton menuElement2 = new JButton("Achievements");
        menuElement2.setForeground(menuElementTextColors.get(MenuElement.Achievements));
        menuElement2.setBackground(menuElementBackgroundColors.get(MenuElement.Achievements));
        menuElement2.setBorderPainted(false);
        menuElement2.setFont(new Font("Arial", Font.BOLD, 25));
        menuElement2.setAlignmentX(Component.CENTER_ALIGNMENT);
        menuElement2.setMaximumSize(new Dimension(244, 60));

        JButton menuElement3 = new JButton("Settings");
        menuElement3.setForeground(menuElementTextColors.get(MenuElement.Settings));
        menuElement3.setBackground(menuElementBackgroundColors.get(MenuElement.Settings));
        menuElement3.setBorderPainted(false);
        menuElement3.setFont(new Font("Arial", Font.BOLD, 25));
        menuElement3.setAlignmentX(Component.CENTER_ALIGNMENT);
        menuElement3.setMaximumSize(new Dimension(244, 60));

        JButton menuElement4 = new JButton("Log out");
        menuElement4.setForeground(menuElementTextColors.get(MenuElement.LogOut));
        menuElement4.setBackground(menuElementBackgroundColors.get(MenuElement.LogOut));
        menuElement4.setBorderPainted(false);
        menuElement4.setFont(new Font("Arial", Font.BOLD, 25));
        menuElement4.setAlignmentX(Component.CENTER_ALIGNMENT);
        menuElement4.setMaximumSize(new Dimension(244, 60));

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(menuElement1);
        this.add(menuElement2);
        this.add(menuElement3);
        this.add(menuElement4);
        this.setBounds(PANEL_X, PANEL_Y, PANEL_WIDTH, PANEL_HEIGHT);
    }

//    private void run(){
//    frame = new JFrame("Habit Tracker");
//        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        frame.setSize(968, 549);
//        frame.setResizable(false);
//        frame.getContentPane().setLayout(null);
//        frame.getContentPane().setVisible(true);
//        frame.getContentPane().add(this);
//        frame.setBackground(new Color(204, 204, 204));
//        frame.setVisible(true);
//    }
}
