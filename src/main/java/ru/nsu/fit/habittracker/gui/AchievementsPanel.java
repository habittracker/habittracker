package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AchievementsPanel extends JPanel {
    private final int CREATE_BUTTON_X = 602;
    private final int CREATE_BUTTON_Y = 456;
    private final int CREATE_BUTTON_WIDTH = 50;
    private final int CREATE_BUTTON_HEIGHT = 50;

    private final int PANEL_X = 269;
    private final int PANEL_Y = 25;
    private final int PANEL_WIDTH = 665;
    private final int PANEL_HEIGHT = 515;
    private final Color PANEL_BACKGROUND = new Color(250, 250, 250);

    private JFrame frame;
    private ImageIcon plusIcon;


    public static void main(String[] args){
       new AchievementsPanel();
    }

    AchievementsPanel()
    {
        plusIcon = new ImageIcon("resources/plus.png", "plus button");
        JButton createAchievementButton = new JButton(plusIcon);
        createAchievementButton.setBounds(CREATE_BUTTON_X, CREATE_BUTTON_Y, CREATE_BUTTON_WIDTH, CREATE_BUTTON_HEIGHT);
        createAchievementButton.setOpaque(false);
        createAchievementButton.setContentAreaFilled(false);
        createAchievementButton.setBorderPainted(false);
        createAchievementButton.addMouseListener(new MouseAdapter()
    {
        @Override
        public void mouseReleased(MouseEvent e)
        {

        }
    });


        this.setBackground(PANEL_BACKGROUND);
        this.setSize(PANEL_WIDTH, PANEL_HEIGHT);
        this.setBounds(PANEL_X, PANEL_Y, PANEL_WIDTH, PANEL_HEIGHT);
        this.setLayout(null);
        this.add(createAchievementButton);

        frame = new JFrame("Habit Tracker");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(968, 588);
        frame.setResizable(false);
        frame.getContentPane().setLayout(null);
        frame.getContentPane().setVisible(true);
        frame.getContentPane().add(this);
        frame.setBackground(new Color(204, 204, 204));
        frame.setVisible(true);
    }
}
