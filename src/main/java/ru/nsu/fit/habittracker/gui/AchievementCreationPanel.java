package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AchievementCreationPanel extends JPanel {
    private final int TITLE_PANEL_X = 152;
    private final int TITLE_PANEL_Y = 134;
    private final int TITLE_PANEL_WIDTH = 419;
    private final int TITLE_PANEL_HEIGHT = 60;
    private final Color TITLE_PANEL_BACKGROUND = new Color(219, 76, 63);

    private final int TITLE_X = 20;
    private final int TITLE_Y = 11;
    private final int TITLE_WIDTH = 305;
    private final int TITLE_HEIGHT = 37;
    private final Color TITLE_TEXT_COLOR = new Color(255, 255, 255);
    private final Font TITLE_FONT = new Font("Arial", Font.BOLD, 32);


    private final Color SHADE_COLOR = new Color(51, 51, 51, 77);
    private final int SHADE_WIDTH = 724;
    private final int SHADE_HEIGHT = 588;
    private final int SHADE_X = 244;
    private final int SHADE_Y = 0;

    private final int FORM_PANEL_X = 152;
    private final int FORM_PANEL_Y = 193;
    private final int FORM_PANEL_WIDTH = 419;
    private final int FORM_PANEL_HEIGHT = 166;
    private final Color FORM_PANEL_BACKGROUND = new Color(242, 242, 242);

    private final int FIELD_LABEL_X = 31;
    private final int FIELD_LABEL_Y = 16;
    private final int FIELD_LABEL_WIDTH = 54;
    private final int FIELD_LABEL_HEIGHT = 24;
    private final Font FIELD_LABEL_FONT = new Font("Arial", Font.BOLD, 20);
    private final Color FIELD_LABEL_COLOR = new Color(51, 51, 51);

    private final int NAME_FIELD_X = 31;
    private final int NAME_FIELD_Y = 44;
    private final int NAME_FIELD_WIDTH = 362;
    private final int NAME_FIELD_HEIGHT = 43;
    private final Font NAME_FIELD_FONT = new Font("Arial", Font.BOLD, 20);
    private final Color NAME_FIELD_COLOR = new Color(51, 51, 51);

    private final int FIELD_NOTE_X = 33;
    private final int FIELD_NOTE_Y = 88;
    private final int FIELD_NOTE_WIDTH = 173;
    private final int FIELD_NOTE_HEIGHT = 15;
    private final Font FIELD_NOTE_FONT = new Font("Arial", Font.PLAIN, 12);
    private final Color FIELD_NOTE_COLOR = new Color(51, 51, 51);

    private final int CANCEL_BUTTON_X = 220;
    private final int CANCEL_BUTTON_Y = 118;
    private final int CANCEL_BUTTON_WIDTH = 130;
    private final int CANCEL_BUTTON_HEIGHT = 29;
    private final Font CANCEL_BUTTON_FONT = new Font("Arial", Font.BOLD, 22);
    private final Color CANCEL_BUTTON_TEXT_COLOR = new Color(51, 51, 51);

    private final int OK_BUTTON_X = 333;
    private final int OK_BUTTON_Y = 118;
    private final int OK_BUTTON_WIDTH = 85;
    private final int OK_BUTTON_HEIGHT = 29;
    private final Font OK_BUTTON_FONT = new Font("Arial", Font.BOLD, 22);
    private final Color OK_BUTTON_TEXT_COLOR = new Color(51, 51, 51);


    private JFrame frame;


    public static void main(String[] args) {
        new AchievementCreationPanel();
    }

    AchievementCreationPanel() {
        JPanel titlePanel = new JPanel(null);
        titlePanel.setBounds(TITLE_PANEL_X, TITLE_PANEL_Y, TITLE_PANEL_WIDTH, TITLE_PANEL_HEIGHT);
        titlePanel.setBackground(TITLE_PANEL_BACKGROUND);
        JLabel title = new JLabel("Create achievement");
        title.setFont(TITLE_FONT);
        title.setMaximumSize(new Dimension(TITLE_WIDTH, TITLE_HEIGHT));
        title.setForeground(TITLE_TEXT_COLOR);
        title.setBounds(TITLE_X, TITLE_Y, TITLE_WIDTH, TITLE_HEIGHT);
        titlePanel.add(title);

        JPanel formPanel = new JPanel(null);
        formPanel.setBounds(FORM_PANEL_X, FORM_PANEL_Y, FORM_PANEL_WIDTH, FORM_PANEL_HEIGHT);
        formPanel.setBackground(FORM_PANEL_BACKGROUND);

        JLabel nameFieldLabel = new JLabel("Name");
        nameFieldLabel.setFont(FIELD_LABEL_FONT);
        nameFieldLabel.setMaximumSize(new Dimension(FIELD_LABEL_WIDTH, FIELD_LABEL_HEIGHT));
        nameFieldLabel.setForeground(FIELD_LABEL_COLOR);
        nameFieldLabel.setBounds(FIELD_LABEL_X, FIELD_LABEL_Y, FIELD_LABEL_WIDTH, FIELD_LABEL_HEIGHT);

        JTextField nameField = new JTextField();
        nameField.setFont(NAME_FIELD_FONT);
        nameField.setMaximumSize(new Dimension(NAME_FIELD_WIDTH, NAME_FIELD_HEIGHT));
        nameField.setForeground(NAME_FIELD_COLOR);
        nameField.setBounds(NAME_FIELD_X, NAME_FIELD_Y, NAME_FIELD_WIDTH, NAME_FIELD_HEIGHT);

        JLabel nameFieldNote = new JLabel("(Must contain 10 or less letters");
        nameFieldNote.setFont(FIELD_NOTE_FONT);
        nameFieldNote.setMaximumSize(new Dimension(FIELD_NOTE_WIDTH, FIELD_LABEL_HEIGHT));
        nameFieldNote.setForeground(FIELD_NOTE_COLOR);
        nameFieldNote.setBounds(FIELD_NOTE_X, FIELD_NOTE_Y, FIELD_NOTE_WIDTH, FIELD_NOTE_HEIGHT);

        JButton cancelButton = new JButton("CANCEL");
        cancelButton.setBounds(CANCEL_BUTTON_X, CANCEL_BUTTON_Y, CANCEL_BUTTON_WIDTH, CANCEL_BUTTON_HEIGHT);
        cancelButton.setFont(CANCEL_BUTTON_FONT);
        cancelButton.setOpaque(false);
        cancelButton.setContentAreaFilled(false);
        cancelButton.setBorderPainted(false);
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        JButton okButton = new JButton("OK");
        okButton.setBounds(OK_BUTTON_X, OK_BUTTON_Y, OK_BUTTON_WIDTH, OK_BUTTON_HEIGHT);
        okButton.setFont(OK_BUTTON_FONT);
        okButton.setOpaque(false);
        okButton.setContentAreaFilled(false);
        okButton.setBorderPainted(false);
        okButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        formPanel.add(nameFieldLabel);
        formPanel.add(nameField);
        formPanel.add(nameFieldNote);
        formPanel.add(cancelButton);
        formPanel.add(okButton);


        this.setBackground(SHADE_COLOR);
        this.setSize(SHADE_WIDTH, SHADE_HEIGHT);
        this.setBounds(SHADE_X, SHADE_Y, SHADE_WIDTH, SHADE_HEIGHT);
        this.setLayout(null);
        this.add(titlePanel);
        this.add(formPanel);

        frame = new JFrame("Habit Tracker");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(968, 588);
        frame.setResizable(false);
        frame.getContentPane().setLayout(null);
        frame.getContentPane().setVisible(true);
        frame.getContentPane().add(this);
        frame.setBackground(new Color(204, 204, 204));
        frame.setVisible(true);
    }
}