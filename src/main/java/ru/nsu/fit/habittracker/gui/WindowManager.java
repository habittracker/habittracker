package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class WindowManager {
	private JFrame currentWindow = new JFrame("Habit Tracker");
	;
	private JPanel currentPanel;
	private JPanel introPanel;
	private JPanel sideBar;
	private JPanel briefCards;
	private JPanel habitCreationPanel;
	private ImageIcon userAvatar = new ImageIcon("resources/avatar2.png", "avatar");

	public static void main(String[] args) {
		WindowManager windowManager = new WindowManager();
		windowManager.run();
	}

	public WindowManager() {
		introPanel = new IntroWindow(this);
		introPanel.setVisible(false);

		LinkedList<Component> components = new LinkedList<>();
		components.add(new PersonalInfoPanel("Michael", userAvatar));
		components.add(new MenuPanel(MenuPanel.MenuElement.Habits));
		sideBar = new SidePanel(components);
		sideBar.setVisible(false);

		briefCards = new HabitBriefCardsPanel(this);
		briefCards.setVisible(false);

		habitCreationPanel = new HabitCreationPanel(this);
		habitCreationPanel.setVisible(false);
	}

	public void setCurrentWindow(int i) {
		switch(i) {
			case 1:
				currentPanel.setVisible(false);
				currentWindow.remove(currentPanel);
				sideBar.setVisible(true);
				currentWindow.setLayout(null);
				currentWindow.add(sideBar);
				currentPanel = briefCards;
				currentPanel.setVisible(true);
				currentWindow.add(currentPanel);
				break;
			case 2:
				habitCreationPanel.setVisible(true);
				currentWindow.add(habitCreationPanel, 0);
				currentWindow.repaint();
				break;
			case 3:
				habitCreationPanel.setVisible(false);
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
			case 7:
				break;
			case 8:
				break;
			case 9:
				break;
			default:
				break;
		}
	}

	public void run() {
		currentPanel = introPanel;
		currentPanel.setVisible(true);
		currentWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		currentWindow.setSize(968, 549);
		currentWindow.add(currentPanel, BorderLayout.CENTER);
		currentWindow.setBackground(new Color(204, 204, 204));
		currentWindow.setResizable(false);
		currentWindow.setVisible(true);
	}

}
