package ru.nsu.fit.habittracker.gui;

import ru.nsu.fit.habittracker.entity.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.*;


public class HabitBriefCard extends JPanel {

    private final Color PRIMARY_COLOR = new Color(51, 51, 51);
    private final Color SECONDARY_COLOR = new Color(102, 102, 102);
    private final Font TEXT_FONT = new Font("Roboto", Font.BOLD, 16);
    private final Font HEADING_FONT = new Font("Arial", Font.BOLD, 32);


    private final int SHOW_BUTTON_X = 498;
    private final int SHOW_BUTTON_Y = 134;
    private final int SHOW_BUTTON_WIDTH = 160;
    private final int SHOW_BUTTON_HEIGHT = 22;

    private final int THUMBSUP_BUTTON_X = 558;
    private final int THUMBSUP_BUTTON_Y = 25;
    private final int THUMBSUP_BUTTON_WIDTH = 35;
    private final int THUMBSUP_BUTTON_HEIGHT = 35;

    private final int THUMBSDOWN_BUTTON_X = 603;
    private final int THUMBSDOWN_BUTTON_Y = 27;
    private final int THUMBSDOWN_BUTTON_WIDTH = 35;
    private final int THUMBSDOWN_BUTTON_HEIGHT = 35;

//    private String habitName;
//    private String relatesToAchievement;
//    private int desiredChainLength;
//    private HashMap<String, Boolean> isHabitDay = new HashMap<>();
//    private boolean[] habitDays = new boolean[7];


    public static void main(String[] args) {

        WeekPracticePlan weekPlan = new WeekPracticePlan();
        weekPlan.setPracticePlan(DayOfWeek.MONDAY, true);
        weekPlan.setPracticePlan(DayOfWeek.TUESDAY, true);
        weekPlan.setPracticePlan(DayOfWeek.WEDNESDAY, true);
        weekPlan.setPracticePlan(DayOfWeek.THURSDAY, true);
        weekPlan.setPracticePlan(DayOfWeek.FRIDAY, true);
        Achievement achievement = new Achievement("Skinny");
        HabitRequirements reqs = new HabitRequirements(weekPlan, new Period(Calendar.getInstance().getTime(),
                                                                            Calendar.getInstance().getTime()));
        Habit habit = new Habit("Don't eat sugary food", reqs, Arrays.asList(achievement),
                                11, 0, 2);

        HabitBriefCard habitBriefCard = new HabitBriefCard(habit);
        JFrame frame = new JFrame("Habit Tracker");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(968, 588);
        frame.setResizable(false);
        frame.getContentPane().setLayout(null);
        frame.getContentPane().setVisible(true);
        frame.getContentPane().add(habitBriefCard);
        frame.setBackground(new Color(204, 204, 204));
        frame.setVisible(true);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawLine(22, 58, 349, 58);
        g.drawLine(515, 154, 640, 154);
    }

    HabitBriefCard(Habit habit) {
        HabitRequirements habitRequirements = habit.getCurrentRequirements();

        JLabel habitNameLabel = new JLabel(habit.getName());
        habitNameLabel.setFont(HEADING_FONT);
        habitNameLabel.setMaximumSize(new Dimension(327, 37));
        habitNameLabel.setForeground(PRIMARY_COLOR);
        habitNameLabel.setBounds(22, 20, 327, 37);

        WeekPracticePlan weekPlan = habitRequirements.getWeekPlan();
        LinkedList<JLabel> weekLabels = new LinkedList<>();
        for (DayOfWeek day : DayOfWeek.values()) {
            JLabel weekLabel = new JLabel(day.getDisplayName(TextStyle.SHORT, Locale.US));
            weekLabel.setFont(TEXT_FONT);
            weekLabel.setMaximumSize(new Dimension(40, 22));
            Color colorText = weekPlan.isPracticePlanned(day) ? PRIMARY_COLOR : SECONDARY_COLOR;
            weekLabel.setForeground(colorText);
            weekLabels.add(weekLabel);
        }

        JLabel relatesToAchievementTitle = new JLabel("Relates to achievement:");
        relatesToAchievementTitle.setFont(TEXT_FONT);
        relatesToAchievementTitle.setMaximumSize(new Dimension(216, 22));
        relatesToAchievementTitle.setForeground(SECONDARY_COLOR);
        relatesToAchievementTitle.setBounds(23, 102, 216, 22);

        java.util.List<Achievement> relatedAchievements = habit.getRelatedAchievements();
        String achievementName = relatedAchievements.isEmpty() ? "None" : relatedAchievements.get(0).getName();
        JLabel relatesToAchievementValue = new JLabel(achievementName);
        relatesToAchievementValue.setFont(TEXT_FONT);
        relatesToAchievementValue.setMaximumSize(new Dimension(60, 22));
        relatesToAchievementValue.setForeground(PRIMARY_COLOR);
        relatesToAchievementValue.setBounds(254, 102, 60, 22);


        JLabel desiredChainLengthTitle = new JLabel("Desired chain length:");
        desiredChainLengthTitle.setFont(TEXT_FONT);
        desiredChainLengthTitle.setMaximumSize(new Dimension(188, 22));
        desiredChainLengthTitle.setForeground(SECONDARY_COLOR);
        desiredChainLengthTitle.setBounds(23, 134, 188, 22);

        JLabel desiredChainLengthValue = new JLabel(Integer.toString(habit.getDesiredChainLength()));
        desiredChainLengthValue.setFont(TEXT_FONT);
        desiredChainLengthValue.setMaximumSize(new Dimension(81, 22));
        desiredChainLengthValue.setForeground(PRIMARY_COLOR);
        desiredChainLengthValue.setBounds(226, 134, 81, 22);

        int x = 22;
        int y = 70;
        for (JLabel weekLabel : weekLabels)
        {
            weekLabel.setBounds(x, y, 40, 22);
            x += 46;
        }

        ImageIcon thumbsUpIcon = new ImageIcon("resources/thumbsUp.png",
                                         "thumbs up");
        ImageIcon thumbsDownIcon =  new ImageIcon("resources/thumbsDown.png",
                                                 "thumbs down");
        JButton yesButton = new JButton(thumbsUpIcon);
        yesButton.setBounds(THUMBSUP_BUTTON_X, THUMBSUP_BUTTON_Y, THUMBSUP_BUTTON_WIDTH, THUMBSUP_BUTTON_HEIGHT);
        yesButton.setOpaque(false);
        yesButton.setContentAreaFilled(false);
        yesButton.setBorderPainted(false);
        yesButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e) {
                Calendar now = Calendar.getInstance();
//                habit.markDay(now.getTime(), PracticeStatus.Success);
            }
        });

        JButton noButton = new JButton(thumbsDownIcon);
        noButton.setBounds(THUMBSDOWN_BUTTON_X, THUMBSDOWN_BUTTON_Y, THUMBSDOWN_BUTTON_WIDTH, THUMBSDOWN_BUTTON_HEIGHT);
        noButton.setOpaque(false);
        noButton.setContentAreaFilled(false);
        noButton.setBorderPainted(false);
        noButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e) {
                Calendar now = Calendar.getInstance();
//                habit.markDay(now.getTime(), PracticeStatus.Fail);
            }
        });

        JButton showButton = new JButton("Show me more!");
        showButton.setBounds(SHOW_BUTTON_X, SHOW_BUTTON_Y, SHOW_BUTTON_WIDTH, SHOW_BUTTON_HEIGHT);
        showButton.setFont(new Font("Roboto", Font.PLAIN, 16));
        showButton.setForeground(PRIMARY_COLOR);
        showButton.setOpaque(false);
        showButton.setContentAreaFilled(false);
        showButton.setBorderPainted(false);
        showButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
            }
        });


        this.setBackground(new Color(250, 250, 250));
        this.setSize(665, 180);
        this.setBounds(268, 25, 665, 180);
        this.setBackground(new Color(250, 250, 250));
        this.setLayout(null);
        this.add(habitNameLabel);

        for (JLabel weekLabel : weekLabels)
        {
            this.add(weekLabel);
        }
        this.add(relatesToAchievementTitle);
        this.add(relatesToAchievementValue);
        this.add(desiredChainLengthTitle);
        this.add(desiredChainLengthValue);
        this.add(yesButton);
        this.add(noButton);
        this.add(showButton);
    }
}
