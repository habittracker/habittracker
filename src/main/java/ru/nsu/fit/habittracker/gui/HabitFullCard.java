package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

public class HabitFullCard extends JPanel {
    private final int PANEL_X = 269;
    private final int PANEL_Y = 25;
    private final int PANEL_WIDTH = 667;
    private final int PANEL_HEIGHT = 514;
    private final Color PANEL_BACKGROUND = new Color(250, 250, 250);

    private final Color PRIMARY_COLOR = new Color(51, 51, 51);
    private final Color SECONDARY_COLOR = new Color(102, 102, 102);
    private final Font TEXT_FONT = new Font("Roboto", Font.BOLD, 18);
    private final Font HEADING_FONT = new Font("Arial", Font.BOLD, 32);

    private final int OPTIONS_BUTTON_X = 628;
    private final int OPTIONS_BUTTON_Y = 27;
    private final int OPTIONS_BUTTON_WIDTH = 32;
    private final int OPTIONS_BUTTON_HEIGHT = 32;

    private JFrame frame;
    private String habitName;
    private String relatesToAchievement;
    private int desiredChainLength;
    private HashMap<String, Boolean> isHabitDay = new HashMap<>();
    private boolean[] habitDays = new boolean[7];
    private int currentChainLength = 5;
    private int maximumChainLength = 14;
    private Icon optionsIcon;
    private JPopupMenu popupMenu = new JPopupMenu();;

    public static void main(String[] args) {
        boolean [] habitDays = {true, true, true, true, true, false, false};
        HabitFullCard habitFullCard = new HabitFullCard("Don't eat sugary food", "Skinny",
                                                           100, habitDays);

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(SECONDARY_COLOR);
        g.drawLine(28, 59, 355, 59);
        g.drawLine(28, 179, 658, 179);


    }

    private void createPopUpMenu(){
        JMenuItem menuItem = new JMenuItem("Edit");
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "Edit clicked!");
            }
        });
        popupMenu.add(menuItem);

        menuItem = new JMenuItem("Delete");
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "Delete clicked!");
            }
        });
        popupMenu.add(menuItem);

        menuItem = new JMenuItem("Back to all habits");
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "Back to all habits clicked!");
            }
        });
        popupMenu.add(menuItem);
    }

    HabitFullCard(String habitName, String relatesToAchievement, int desiredChainLength, boolean[] habitDays) {
        this.habitName = habitName;
        this.relatesToAchievement = relatesToAchievement;
        this.desiredChainLength = desiredChainLength;
        this.habitDays = habitDays;

        createPopUpMenu();

        optionsIcon = new ImageIcon("resources/menu.png", "options icon");
        JButton optionsButton = new JButton(optionsIcon);
        optionsButton.setBounds(OPTIONS_BUTTON_X, OPTIONS_BUTTON_Y, OPTIONS_BUTTON_WIDTH, OPTIONS_BUTTON_HEIGHT);
        optionsButton.setOpaque(false);
        optionsButton.setContentAreaFilled(false);
        optionsButton.setBorderPainted(false);
        optionsButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                popupMenu.show(e.getComponent(),
                           e.getX(), e.getY());
            }
        });


        JLabel habitNameLabel = new JLabel(habitName);
        habitNameLabel.setFont(HEADING_FONT);
        habitNameLabel.setMaximumSize(new Dimension(327, 37));
        habitNameLabel.setForeground(PRIMARY_COLOR);
        habitNameLabel.setBounds(28, 20, 327, 37);

        LinkedList<JLabel> weekLabels = new LinkedList<>();
        String[] weekDays = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
        for (int i = 0; i < habitDays.length; ++i) {
            JLabel weekLabel = new JLabel(weekDays[i]);
            weekLabel.setFont(TEXT_FONT);
            weekLabel.setMaximumSize(new Dimension(40, 22));
            Color colorText = habitDays[i] ? PRIMARY_COLOR :  SECONDARY_COLOR;
            weekLabel.setForeground(colorText);
            weekLabels.add(weekLabel);
        }

        JLabel relatesToAchievementTitle = new JLabel("Relates to achievement:");
        relatesToAchievementTitle.setFont(TEXT_FONT);
        relatesToAchievementTitle.setMaximumSize(new Dimension(216, 22));
        relatesToAchievementTitle.setForeground(SECONDARY_COLOR);
        relatesToAchievementTitle.setBounds(29, 102, 216, 22);

        JLabel relatesToAchievementValue = new JLabel(relatesToAchievement);
        relatesToAchievementValue.setFont(TEXT_FONT);
        relatesToAchievementValue.setMaximumSize(new Dimension(60, 22));
        relatesToAchievementValue.setForeground(PRIMARY_COLOR);
        relatesToAchievementValue.setBounds(260, 102, 60, 22);


        JLabel desiredChainLengthTitle = new JLabel("Desired chain length:");
        desiredChainLengthTitle.setFont(TEXT_FONT);
        desiredChainLengthTitle.setMaximumSize(new Dimension(188, 22));
        desiredChainLengthTitle.setForeground(SECONDARY_COLOR);
        desiredChainLengthTitle.setBounds(29, 134, 188, 22);

        JLabel desiredChainLengthValue = new JLabel(Integer.toString(desiredChainLength));
        desiredChainLengthValue.setFont(TEXT_FONT);
        desiredChainLengthValue.setMaximumSize(new Dimension(81, 22));
        desiredChainLengthValue.setForeground(PRIMARY_COLOR);
        desiredChainLengthValue.setBounds(232, 134, 81, 22);



        JLabel currentChainLengthTitle = new JLabel("Current chain length:");
        currentChainLengthTitle.setFont(TEXT_FONT);
        currentChainLengthTitle.setMaximumSize(new Dimension(187, 22));
        currentChainLengthTitle.setForeground(SECONDARY_COLOR);
        currentChainLengthTitle.setBounds(365, 102, 187, 22);

        JLabel currentChainLengthValue = new JLabel(Integer.toString(currentChainLength));
        currentChainLengthValue.setFont(TEXT_FONT);
        currentChainLengthValue.setMaximumSize(new Dimension(59, 22));
        currentChainLengthValue.setForeground(PRIMARY_COLOR);
        currentChainLengthValue.setBounds(568, 102, 59, 22);



        JLabel maximumChainLengthTitle = new JLabel("Maximum chain length:");
        maximumChainLengthTitle.setFont(TEXT_FONT);
        maximumChainLengthTitle.setMaximumSize(new Dimension(188, 22));
        maximumChainLengthTitle.setForeground(SECONDARY_COLOR);
        maximumChainLengthTitle.setBounds(365, 134, 208, 22);

        JLabel maximumChainLengthValue = new JLabel(Integer.toString(maximumChainLength));
        maximumChainLengthValue.setFont(TEXT_FONT);
        maximumChainLengthValue.setMaximumSize(new Dimension(70, 22));
        maximumChainLengthValue.setForeground(PRIMARY_COLOR);
        maximumChainLengthValue.setBounds(588, 134, 70, 22);

        int x = 28;
        int y = 70;
        for (JLabel weekLabel : weekLabels)
        {
            weekLabel.setBounds(x, y, 40, 22);
            x += 46;
        }

        this.setSize(PANEL_WIDTH, PANEL_HEIGHT);
        this.setBounds(PANEL_X, PANEL_Y, PANEL_WIDTH, PANEL_HEIGHT);
        this.setBackground(PANEL_BACKGROUND);
        this.setLayout(null);
        this.add(habitNameLabel);

        for (JLabel weekLabel : weekLabels)
        {
            this.add(weekLabel);
        }
        this.add(relatesToAchievementTitle);
        this.add(relatesToAchievementValue);
        this.add(desiredChainLengthTitle);
        this.add(desiredChainLengthValue);
        this.add(currentChainLengthTitle);
        this.add(currentChainLengthValue);
        this.add(maximumChainLengthTitle);
        this.add(maximumChainLengthValue);
        this.add(optionsButton);

        frame = new JFrame("Habit Tracker");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(968, 588);
        frame.setResizable(false);
        frame.getContentPane().setLayout(null);
        frame.getContentPane().setVisible(true);
        frame.getContentPane().add(this);
        frame.setBackground(new Color(204, 204, 204));
        frame.setVisible(true);
    }

    public void setHabitName(String habitName)
    {
        this.habitName = habitName;
    }

    public void setRelatesToAchievement(String relatesToAchievement)
    {
        this.relatesToAchievement = relatesToAchievement;
    }

    public void setDesiredChainLength(int desiredChainLength)
    {
        this.desiredChainLength = desiredChainLength;
    }

    public String getHabitName()
    {
        return habitName;
    }

    public String getRelatesToAchievement()
    {
        return relatesToAchievement;
    }

    public int getDesiredChainLength()
    {
        return desiredChainLength;
    }
}
