package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AchievementView extends JPanel{
    private final Color BORDER_COLOR = new Color(121, 121, 121);
    private final Color INDICATOR_COLOR = new Color(160, 202, 147);

    private final int NAME_X = 25;
    private final int NAME_Y = 41;
    private final int NAME_WIDTH = 165;
    private final int NAME_HEIGHT = 29;
    private final Color NAME_TEXT_COLOR = new Color(51, 51, 51);
    private final Font NAME_FONT = new Font("Arial", Font.BOLD, 25);

    private final int POINTS_X = 542;
    private final int POINTS_Y = 47;
    private final int POINTS_WIDTH = 86;
    private final int POINTS_HEIGHT = 22;
    private final Color POINTS_TEXT_COLOR = new Color(102, 102, 102);
    private final Font POINTS_FONT = new Font("Arial", Font.BOLD, 18);

    private final int ACHIEVEMENTS_PANEL_X = 269;
    private final int ACHIEVEMENTS_PANEL_Y = 25;
    private final int ACHIEVEMENTS_PANEL_WIDTH = 665;
    private final int ACHIEVEMENTS_PANEL_HEIGHT = 515;
    private final Color ACHIEVEMENTS_PANEL_BACKGROUND = new Color(250, 250, 250);

    private final int CLOSE_BUTTON_X = 638;
    private final int CLOSE_BUTTON_Y = 49;
    private final int CLOSE_BUTTON_WIDTH = 15;
    private final int CLOSE_BUTTON_HEIGHT = 15;

    private final int POINTS_PROGRESS_BOX_X =  210;
    private final int POINTS_PROGRESS_BOX_Y =  41;
    private final double POINTS_PROGRESS_BOX_WIDTH =  320.0;
    private final int POINTS_PROGRESS_BOX_HEIGHT =  31;


    private final int WHOLE_POINTS_COUNT = 1000;

    private String name;
    private int points;
    private JFrame frame;
    private ImageIcon closeIcon;

    public static void main(String[] args){
        new AchievementView("Well-groomed", 1000);
    }

    AchievementView(String name, int points){
        this.name = name;
        this.points = points;

        JLabel achievementNameLabel = new JLabel(name);
        achievementNameLabel.setFont(NAME_FONT);
        achievementNameLabel.setMaximumSize(new Dimension(NAME_WIDTH, NAME_HEIGHT));
        achievementNameLabel.setForeground(NAME_TEXT_COLOR);
        achievementNameLabel.setBounds(NAME_X, NAME_Y, NAME_WIDTH, NAME_HEIGHT);

        JLabel pointsLabel = new JLabel(Integer.toString(points) + "/1000");
        pointsLabel.setFont(POINTS_FONT);
        pointsLabel.setMaximumSize(new Dimension(POINTS_WIDTH, POINTS_HEIGHT));
        pointsLabel.setForeground(POINTS_TEXT_COLOR);
        pointsLabel.setBounds(POINTS_X, POINTS_Y, POINTS_WIDTH, POINTS_HEIGHT);

        JPanel externalPointsProgressBox = new JPanel();
        externalPointsProgressBox.setBackground(Color.WHITE);
        externalPointsProgressBox.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        externalPointsProgressBox.setSize(new Dimension((int)POINTS_PROGRESS_BOX_WIDTH, POINTS_PROGRESS_BOX_HEIGHT));
        externalPointsProgressBox.setBounds(POINTS_PROGRESS_BOX_X, POINTS_PROGRESS_BOX_Y,
                                            (int)POINTS_PROGRESS_BOX_WIDTH, POINTS_PROGRESS_BOX_HEIGHT);
        externalPointsProgressBox.setVisible(true);

        JPanel internalPointsProgressBox = new JPanel();
        internalPointsProgressBox.setBackground(INDICATOR_COLOR);
        internalPointsProgressBox.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        int internalPointsProgressBoxWidth = (int)(POINTS_PROGRESS_BOX_WIDTH / WHOLE_POINTS_COUNT * points);
        internalPointsProgressBox.setSize(new Dimension(internalPointsProgressBoxWidth, 31));
        internalPointsProgressBox.setBounds(POINTS_PROGRESS_BOX_X, POINTS_PROGRESS_BOX_Y,
                                            internalPointsProgressBoxWidth, POINTS_PROGRESS_BOX_HEIGHT);
        internalPointsProgressBox.setVisible(true);

        closeIcon = new ImageIcon("resources/close.png", "close button");
        JButton closeButton = new JButton(closeIcon);
        closeButton.setBounds(CLOSE_BUTTON_X, CLOSE_BUTTON_Y, CLOSE_BUTTON_WIDTH, CLOSE_BUTTON_HEIGHT);
        closeButton.setOpaque(false);
        closeButton.setContentAreaFilled(false);
        closeButton.setBorderPainted(false);
        closeButton.addMouseListener(new MouseAdapter()   {
            @Override
            public void mouseReleased(MouseEvent e)
            {

            }
        });

        this.setBackground(ACHIEVEMENTS_PANEL_BACKGROUND);
        this.setSize(ACHIEVEMENTS_PANEL_WIDTH, ACHIEVEMENTS_PANEL_HEIGHT);
        this.setBounds(ACHIEVEMENTS_PANEL_X, ACHIEVEMENTS_PANEL_Y, ACHIEVEMENTS_PANEL_WIDTH, ACHIEVEMENTS_PANEL_HEIGHT);
        this.setLayout(null);
        this.add(achievementNameLabel);
        this.add(pointsLabel);
        this.add(closeButton);
        this.add(internalPointsProgressBox);
        this.add(externalPointsProgressBox);

        frame = new JFrame("Habit Tracker");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(968, 588);
        frame.setResizable(false);
        frame.getContentPane().setLayout(null);
        frame.getContentPane().setVisible(true);
        frame.getContentPane().add(this);
        frame.setBackground(new Color(204, 204, 204));
        frame.setVisible(true);
    }
}
