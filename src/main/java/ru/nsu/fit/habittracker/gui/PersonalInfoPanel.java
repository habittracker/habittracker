package ru.nsu.fit.habittracker.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class PersonalInfoPanel extends JPanel {
    private final int PANEL_X = 0;
    private final int PANEL_Y = 0;
    private final int PANEL_WIDTH = 244;
    private final int PANEL_HEIGHT = 227;
    private final Color BACKGROUND = new Color(204, 204, 204);

    private final int USERNAME_X = 60;
    private final int USERNAME_Y = 179;
    private final int USERNAME_WIDTH = 121;
    private final int USERNAME_HEIGHT = 37;
    private final Color USERNAME_TEXT_COLOR =new Color(51, 51, 51);
    private final Font USERNAME_FONT = new Font("Arial", Font.BOLD, 32);

    private final int AVATAR_X = 18;
    private final int AVATAR_Y = 5;
    private final int AVATAR_WIDTH = 204;
    private final int AVATAR_HEIGHT = 174;

    private JFrame frame;
    private String userName;
    private ImageIcon userAvatar;


    public static void main(String[] args) {
        ImageIcon userAvatar = new ImageIcon("resources/avatar2.png", "avatar");
        PersonalInfoPanel personalInfoPanel = new PersonalInfoPanel("Michael", userAvatar);
    }


    PersonalInfoPanel(String userName, ImageIcon userAvatar) {
        this.userName = userName;
        this.userAvatar = userAvatar;

        JButton avatar = new JButton(userAvatar);
        avatar.setBounds(AVATAR_X, AVATAR_Y, AVATAR_WIDTH, AVATAR_HEIGHT);
        avatar.setOpaque(false);
        avatar.setContentAreaFilled(false);
        avatar.setBorderPainted(false);

        this.setBackground(BACKGROUND);
        this.setSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        this.setBounds(PANEL_X, PANEL_Y, PANEL_WIDTH, PANEL_HEIGHT);

        JLabel userNameLabel = new JLabel(userName);
        userNameLabel.setFont(USERNAME_FONT);
        userNameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        userNameLabel.setMaximumSize(new Dimension(USERNAME_WIDTH, USERNAME_HEIGHT));
        userNameLabel.setForeground(USERNAME_TEXT_COLOR);
        userNameLabel.setBounds(USERNAME_X, USERNAME_Y, USERNAME_WIDTH, USERNAME_HEIGHT);

        this.setLayout(null);
        this.add(userNameLabel);
        this.add(avatar);

//        frame = new JFrame("Habit Tracker");
//        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        frame.setSize(968, 549);
//        frame.setResizable(false);
//        frame.getContentPane().setLayout(null);
//        frame.getContentPane().setVisible(true);
//        frame.getContentPane().add(this);
//        frame.setBackground(BACKGROUND);
//        frame.setVisible(true);
    }
}
