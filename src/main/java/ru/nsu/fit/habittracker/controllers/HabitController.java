package ru.nsu.fit.habittracker.controllers;

import ru.nsu.fit.habittracker.dal.AchievementStorage;
import ru.nsu.fit.habittracker.dal.HabitStorage;
import ru.nsu.fit.habittracker.dal.SameStatusPeriodStorage;
import ru.nsu.fit.habittracker.entity.Achievement;
import ru.nsu.fit.habittracker.entity.Habit;
import ru.nsu.fit.habittracker.entity.PracticeStatus;
import ru.nsu.fit.habittracker.entity.SameStatusPeriod;

import java.util.Calendar;
import java.util.Date;

public class HabitController {
	private static final int POINTS_PER_DAY = 5;

	private PracticeStatusController practiceStatusController;
	private AchievementStorage achievementStorage;
	private HabitStorage habitStorage;
	private Habit habit;

	public void markDay(Date day, PracticeStatus status) {
		int pointsDelta = 0;

		SameStatusPeriod period = practiceStatusController.findPeriodContaining(day);
		if(status == period.getStatus()) {
			return;
		}



//		final Date today = Calendar.getInstance().getTime();
//		if(today.equals(day)) {
//			if(status == PracticeStatus.Success) {
//				pointsDelta = POINTS_PER_DAY;
//				habit.setCurrentChainLength(habit.getCurrentChainLength() + 1);
//			}
//		} else {
//			pointsDelta = (status == PracticeStatus.Success) ? 2 * POINTS_PER_DAY : -POINTS_PER_DAY;
//		}

		for(Achievement achievement : habit.getRelatedAchievements()) {
			achievement.addPoints(pointsDelta);
			achievementStorage.alterAchievement(achievement);
		}

		habitStorage.alterHabit(habit);
	}

	private static class PracticeStatusController {
		private SameStatusPeriodStorage sameStatusPeriodStorage;
		private  Habit habit;


		public SameStatusPeriod findPeriodContaining(Date day) {
			return null;
		}

		public void setDayStatus(Date day, PracticeStatus status) {

		}
	}
}
