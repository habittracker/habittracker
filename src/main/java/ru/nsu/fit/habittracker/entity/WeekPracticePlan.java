package ru.nsu.fit.habittracker.entity;

import java.time.DayOfWeek;

public class WeekPracticePlan {
	private boolean[] dailyPlans = new boolean[7];

	public void setPracticePlan(DayOfWeek day, boolean shouldPractice) {
		dailyPlans[getDayIndex(day)] = shouldPractice;
	}

	public boolean isPracticePlanned(DayOfWeek day) {
		return dailyPlans[getDayIndex(day)];
	}

	private static int getDayIndex(DayOfWeek day) {
		return day.getValue() - DayOfWeek.MONDAY.getValue();
	}
}
