package ru.nsu.fit.habittracker.entity;

public class HabitRequirements {
	private WeekPracticePlan weekPlan;
	private Period period;

	public HabitRequirements(WeekPracticePlan weekPlan, Period period) {
		this.weekPlan = weekPlan;
		this.period = period;
	}

	public WeekPracticePlan getWeekPlan() {
		return weekPlan;
	}

	public Period getPeriod() {
		return period;
	}

	public void setWeekPlan(WeekPracticePlan weekPlan) {
		this.weekPlan = weekPlan;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}
}
