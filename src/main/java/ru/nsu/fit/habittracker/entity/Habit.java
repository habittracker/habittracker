package ru.nsu.fit.habittracker.entity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Habit {
	private String name;
	private HabitRequirements currentRequirements;
	private List<Achievement> relatedAchievements;
	private int desiredChainLength;
	private int currentChainLength;
	private int maxChainLength;

	public Habit(String name,
	             HabitRequirements currentRequirements,
	             int desiredChainLength) {
		this.name = name;
		this.currentRequirements = currentRequirements;
		this.relatedAchievements = new LinkedList<>();
		this.desiredChainLength = desiredChainLength;
		this.currentChainLength = 0;
		this.maxChainLength = 0;
	}

	public Habit(String name,
	             HabitRequirements currentRequirements,
	             List<Achievement> relatedAchievements,
	             int desiredChainLength,
	             int currentChainLength,
	             int maxChainLength) {
		this.name = name;
		this.currentRequirements = currentRequirements;
		this.relatedAchievements = relatedAchievements;
		this.desiredChainLength = desiredChainLength;
		this.currentChainLength = currentChainLength;
		this.maxChainLength = maxChainLength;
	}

	public String getName() {
		return name;
	}

	public HabitRequirements getCurrentRequirements() {
		return currentRequirements;
	}

	public List<Achievement> getRelatedAchievements() {
		return relatedAchievements;
	}

	public int getDesiredChainLength() {
		return desiredChainLength;
	}

	public int getCurrentChainLength() {
		return currentChainLength;
	}

	public int getMaxChainLength() {
		return maxChainLength;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCurrentRequirements(HabitRequirements currentRequirements) {
		this.currentRequirements = currentRequirements;
	}

	public void setRelatedAchievements(List<Achievement> relatedAchievements) {
		this.relatedAchievements = relatedAchievements;
	}

	public void setCurrentChainLength(int currentChainLength) {
		this.currentChainLength = currentChainLength;
		this.maxChainLength = Math.max(currentChainLength, maxChainLength);
	}
}
