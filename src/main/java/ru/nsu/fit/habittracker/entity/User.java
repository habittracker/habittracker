package ru.nsu.fit.habittracker.entity;

import java.nio.file.Path;

public class User {
	private String name;
	private Path imagePath;

	public User(String name, Path imagePath) {
		this.name = name;
		this.imagePath = imagePath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Path getImagePath() {
		return imagePath;
	}

	public void setImagePath(Path imagePath) {
		this.imagePath = imagePath;
	}
}
