package ru.nsu.fit.habittracker.entity;

public class Achievement {
	public static int MIN_POINTS = 0;
	public static int MAX_POINTS = 1000;

	private String name;
	private int points;

	public Achievement(String name) {
		this.name = name;
		this.points = MIN_POINTS;
	}

	public Achievement(String name, int points) {
		this.name = name;
		this.points = points;
	}

	public String getName() {
		return name;
	}

	public int getPoints() {
		return points;
	}

	public void addPoints(int delta) {
		points = Math.max(MIN_POINTS, Math.min(MAX_POINTS, points + delta));
	}
}
