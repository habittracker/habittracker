package ru.nsu.fit.habittracker.entity;

import java.util.Date;

public class SameStatusPeriod extends Period {
	private PracticeStatus status;

	public SameStatusPeriod(Date start, Date end) {
		super(start, end);
	}

	public void setStatus(PracticeStatus status) {
		this.status = status;
	}

	public PracticeStatus getStatus() {
		return status;
	}
}
